FROM node:10.13.0
# Install all peer dependencies:
RUN yarn global add @discordjs/uws bufferutil erlpack@discordapp/erlpack \
    libsodium-wrappers node-opus opusscript sodium sequelize sqlite3 \
    bufferutil uws
COPY / /root/Sweeper-Bot
WORKDIR /root/Sweeper-Bot
RUN yarn install
# This is a dirty little hack to go around a syntax error I'm getting from the @yamdbf/core package.
# TODO: Remove it as soon as there is a way to make the package compile without it.
RUN sed -i -E "s/.*on.event\:.*this\;//" node_modules/@yamdbf/core/bin/client/Client.d.ts
RUN yarn gulp && mv bin/* .
ENV HOME /root/Sweeper-Bot
CMD ["/bin/sh", "-c", "yarn start > server.log 2>&1"]
