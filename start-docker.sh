#!/bin/bash
docker rm -f siabot
docker start db
docker run -it --link db:db --name siabot --restart=always -d siabot
